package com.cjn.iterator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 上午11:14:45
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConcreteAggregate ca = new ConcreteAggregate();
		
		ca.addItem(0, "aaaa");
		ca.addItem(1, "bbbb");
		ca.addItem(2, "cccc");
		ca.addItem(3, "dddd");
		ca.addItem(4, "eeee");
		ca.addItem(5, "ffff");
		
		System.out.println("正序迭代输出：");
		MyIterator i = new ConcreteMyIterator(ca);
		
		Object obc = i.first();
		System.out.println("第一个元素："+obc.toString());
		while(!i.isDone()){
			System.out.println(i.currentItem()+",请买票");
			i.nest();
		}
		System.out.println("倒序迭代输出：");
		MyIterator idesc = new ConcreteMyIteratorDESC(ca);
		
		Object obj = idesc.first();
		System.out.println("第一个元素："+obj.toString());
		while(!idesc.isDone()){
			System.out.println(idesc.currentItem()+",请买票");
			idesc.nest();
		}
		
	}

}
