package com.cjn.iterator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 上午10:43:46
*@remark 
*
**/
public class ConcreteMyIterator extends MyIterator {
	
	private ConcreteAggregate aggregate;
	private int current = 0;
	
	public ConcreteMyIterator(ConcreteAggregate aggregate) {
		// TODO Auto-generated constructor stub
		this.aggregate =aggregate;
	}
	
	
	@Override
	public Object first() {
		// TODO Auto-generated method stub
		return aggregate.getItem(0);
	}

	@Override
	public Object nest() {
		// TODO Auto-generated method stub
		Object obj = null;
		current++;
		if(current < aggregate.count()){
			
			obj = aggregate.getItem(current);
		}
		return obj;
	}

	@Override
	public Boolean isDone() {
		// TODO Auto-generated method stub
		return current >= aggregate.count() ? true : false;
	}

	@Override
	public Object currentItem() {
		// TODO Auto-generated method stub
		return aggregate.getItem(current);
	}

}
