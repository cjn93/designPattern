package com.cjn.iterator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 上午10:43:46
*@remark 
*
**/
public class ConcreteMyIteratorDESC extends MyIterator {
	
	private ConcreteAggregate aggregate;
	private int current = 0;
	
	public ConcreteMyIteratorDESC(ConcreteAggregate aggregate) {
		// TODO Auto-generated constructor stub
		this.aggregate =aggregate;
		current = aggregate.count() -1;
	}
	
	
	@Override
	public Object first() {
		// TODO Auto-generated method stub
		return aggregate.getItem(aggregate.count()-1);
	}

	@Override
	public Object nest() {
		// TODO Auto-generated method stub
		Object obj = null;
		current--;
		if(current >= 0 ){
			
			obj = aggregate.getItem(current);
		}
		return obj;
	}

	@Override
	public Boolean isDone() {
		// TODO Auto-generated method stub
		return current < 0 ? true : false;
	}

	@Override
	public Object currentItem() {
		// TODO Auto-generated method stub
		return aggregate.getItem(current);
	}

}
