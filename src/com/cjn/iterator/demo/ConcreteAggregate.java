package com.cjn.iterator.demo;

import java.util.*;


/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 上午10:51:52
*@remark 
*
**/
public class ConcreteAggregate extends Aggregate {
	
	private List<Object> items = new ArrayList<>();
	

	@Override
	public MyIterator createIterator() {
		// TODO Auto-generated method stub
		return new ConcreteMyIterator(this);
	}
	
	public int count(){
		return items.size();
	}


	public Object getItem(int index) {
		return items.get(index);
	}
	public void addItem(int index,Object value) {
		items.add(index, value);
	}
	
	

}
