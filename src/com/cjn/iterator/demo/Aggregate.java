package com.cjn.iterator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 上午10:37:23
*@remark 
*
**/
public abstract class Aggregate {
	
	public abstract MyIterator createIterator();

}
