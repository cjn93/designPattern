package com.cjn.iterator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 上午10:23:01
*@remark 
*
**/
public abstract class MyIterator {
		
	public abstract Object first();
	public abstract Object nest();
	public abstract Boolean isDone();
	public abstract Object currentItem();
	
	
}
