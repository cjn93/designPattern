package com.cjn.visitor.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:57:29
*@remark  能枚举他的元素，可以提供一个高层的接口以允许访问者访问他的元素
*
**/

import java.util.*;


public class ObjectStructure {
	
	private List<Element> list = new ArrayList<>();
	
	//增加
	public void addElement(Element element){
		list.add(element);
	}
	
	//删除
	public void removeElement(Element element){
		list.remove(element);
	}
	
	//查看显示
	public void show(Visitor visitor){
		for (Element element : list) {
			element.accept(visitor);
		}
	}
	

}
