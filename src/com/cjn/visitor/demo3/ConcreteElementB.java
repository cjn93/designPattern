package com.cjn.visitor.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午4:17:49
*@remark 具体元素，实现accept操作
*
**/
public class ConcreteElementB extends Element {

	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		visitor.visitConcreteElementB(this);
	}

}
