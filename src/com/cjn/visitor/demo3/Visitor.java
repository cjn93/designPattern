package com.cjn.visitor.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午4:17:12
*@remark 为该对象结构中ConcreteElement 的每一个类声明一个visit操作
*
**/
public abstract class Visitor {
	
	
	public abstract void visitConcreteElementA(ConcreteElementA concreteElementA);
	
	public abstract void visitConcreteElementB(ConcreteElementB concreteElementB);

}
