package com.cjn.visitor.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午4:19:27
*@remark 具体访问者，实现每个由visitor声明的操作。每个操作实现算法的一部分，而该算法片段乃是对应用于结构中对象的类
*
**/
public class ConcreteVisitorB extends Visitor {

	@Override
	public void visitConcreteElementA(ConcreteElementA concreteElementA) {
		// TODO Auto-generated method stub
		System.out.println(concreteElementA.getClass().getName()+" 被 "+this.getClass().getName()+" 访问了");
	}

	@Override
	public void visitConcreteElementB(ConcreteElementB concreteElementB) {
		// TODO Auto-generated method stub
		System.out.println(concreteElementB.getClass().getName()+" 被 "+this.getClass().getName()+" 访问了");
	}

}
