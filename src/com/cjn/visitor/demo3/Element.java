package com.cjn.visitor.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午4:27:45
*@remark 定义一个accept操作，他以一个访问者为参数
*
**/
public abstract class Element {
	
	public abstract void accept(Visitor visitor);

}
