package com.cjn.visitor.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午4:32:33
*@remark  模拟客户端
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ObjectStructure obj = new ObjectStructure();
		
	
		obj.addElement(new ConcreteElementA());
		obj.addElement(new ConcreteElementB());
		
		
		ConcreteVisitorA v1 = new ConcreteVisitorA();
		ConcreteVisitorB v2 = new ConcreteVisitorB();
		
		obj.show(v1);
		obj.show(v2);
		
		
		
		
	}

}
