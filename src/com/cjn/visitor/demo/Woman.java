package com.cjn.visitor.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 上午11:08:16
*@remark 
*
**/
public class Woman extends Person {
	
	private String name;

	public Woman(String name) {
		// TODO Auto-generated constructor stub
		this.name= name;
	}
	
	@Override
	public void getConclusion() {
		// TODO Auto-generated method stub
		if("成功".equals(action)){
			System.out.println(name+"，"+action+"时，背后一定有个不成功的男人");
		}else if("失败".equals(action)){
			System.out.println(name+"，"+action+"时，眼泪汪汪，谁也劝不好");
		}else if("恋爱".equals(action)){
			System.out.println(name+"，"+action+"时，凡事懂也装不懂");
		}
	}

}
