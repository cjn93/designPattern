package com.cjn.visitor.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 上午11:08:16
*@remark 
*
**/
public class Man extends Person {
	
	private String name;

	public Man(String name) {
		// TODO Auto-generated constructor stub
		this.name= name;
	}
	
	@Override
	public void getConclusion() {
		// TODO Auto-generated method stub
		if("成功".equals(action)){
			System.out.println(name+"，"+action+"时，背后多有一个伟大的女人");
		}else if("失败".equals(action)){
			System.out.println(name+"，"+action+"时，闷头喝酒，谁也劝不动");
		}else if("恋爱".equals(action)){
			System.out.println(name+"，"+action+"时，凡事不懂也要装懂");
		}
	}

}
