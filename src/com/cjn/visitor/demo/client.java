package com.cjn.visitor.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:40:33
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		Man man = new Man("男人");
		man.setAction("成功");
		man.getConclusion();
		man.setAction("失败");
		man.getConclusion();
		man.setAction("恋爱");
		man.getConclusion();
		
		Woman woman = new Woman("女人");
		woman.setAction("成功");
		woman.getConclusion();
		woman.setAction("失败");
		woman.getConclusion();
		woman.setAction("恋爱");
		woman.getConclusion();
		
		
		
	}

}
