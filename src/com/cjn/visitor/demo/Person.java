package com.cjn.visitor.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 上午10:00:21
*@remark 
*
**/
public abstract class Person {
	
	protected String action;
	
	
	
	public abstract void getConclusion();


	//set and get
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

}
