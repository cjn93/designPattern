package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:44:50
*@remark 
*
**/
public abstract class Person {
	
	public abstract void accept(Action action);

}
