package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:44:58
*@remark 
*
**/
public abstract class Action {
	
	public abstract void getManConclusion(Man man);
	
	public abstract void getWomanConclusion(Woman woman);


}
