package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:46:21
*@remark 
*
**/
public class Marriage extends Action{
	
	private String action;
	
	public Marriage(String action) {
		// TODO Auto-generated constructor stub
		this.action =action;
	}


	@Override
	public void getManConclusion(Man man) {
		// TODO Auto-generated method stub
		
		System.out.println(man.getName()+"，"+action+"时，感慨到，恋爱游戏终结时，“有妻徒刑”遥无期");
	}

	@Override
	public void getWomanConclusion(Woman woman) {
		// TODO Auto-generated method stub
		System.out.println(woman.getName()+"，"+action+"时，欣慰曰，爱情长跑路漫漫，婚姻保险保平安");
	}

}
