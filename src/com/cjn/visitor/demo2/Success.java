package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:46:21
*@remark 
*
**/
public class Success extends Action{
	
	private String action;
	
	public Success(String action) {
		// TODO Auto-generated constructor stub
		this.action =action;
	}


	@Override
	public void getManConclusion(Man man) {
		// TODO Auto-generated method stub
		System.out.println(man.getName()+"，"+action+"时，背后多有一个伟大的女人");
	}

	@Override
	public void getWomanConclusion(Woman woman) {
		// TODO Auto-generated method stub
		System.out.println(woman.getName()+"，"+action+"时，背后一定有个不成功的男人");
	}

}
