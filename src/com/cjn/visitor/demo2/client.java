package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午3:01:12
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ObjectStructure obj = new ObjectStructure();
		
		obj.addPerson(new Man("男人"));
		obj.addPerson(new Woman("女人"));
		
		Success success = new Success("成功");
		obj.show(success);

		Failing failing = new Failing("失败");
		obj.show(failing);
		
		Love love = new Love("恋爱");
		obj.show(love);
		
		Marriage marriage = new Marriage("结婚");
		obj.show(marriage);
		
		
	}

}
