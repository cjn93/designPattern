package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 上午11:08:16
*@remark 
*
**/
public class Man extends Person {
	
	private String name;

	public Man(String name) {
		// TODO Auto-generated constructor stub
		this.name= name;
	}
	@Override
	public void accept(Action action) {
		// TODO Auto-generated method stub
		action.getManConclusion(this);
	}
	
	//set and get
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



	

}
