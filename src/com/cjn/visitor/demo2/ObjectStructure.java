package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:57:29
*@remark 
*
**/

import java.util.*;


public class ObjectStructure {
	
	private List<Person> list = new ArrayList<>();
	
	//增加
	public void addPerson(Person person){
		list.add(person);
	}
	
	//删除
	public void removePerson(Person person){
		list.remove(person);
	}
	
	//查看显示
	public void show(Action action){
		for (Person person : list) {
			person.accept(action);
		}
	}
	

}
