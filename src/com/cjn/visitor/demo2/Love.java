package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:46:21
*@remark 
*
**/
public class Love extends Action{
	
	private String action;
	
	public Love(String action) {
		// TODO Auto-generated constructor stub
		this.action =action;
	}


	@Override
	public void getManConclusion(Man man) {
		// TODO Auto-generated method stub
		
		System.out.println(man.getName()+"，"+action+"时，凡事不懂也要装懂");
	}

	@Override
	public void getWomanConclusion(Woman woman) {
		// TODO Auto-generated method stub
		System.out.println(woman.getName()+"，"+action+"时，凡事懂也装不懂");
	}

}
