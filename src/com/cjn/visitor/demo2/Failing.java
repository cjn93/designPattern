package com.cjn.visitor.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月27日 下午2:46:21
*@remark 
*
**/
public class Failing extends Action{
	
	private String action;
	
	public Failing(String action) {
		// TODO Auto-generated constructor stub
		this.action =action;
	}


	@Override
	public void getManConclusion(Man man) {
		// TODO Auto-generated method stub
		
		System.out.println(man.getName()+"，"+action+"时，闷头喝酒，谁也劝不动");
	}

	@Override
	public void getWomanConclusion(Woman woman) {
		// TODO Auto-generated method stub
		System.out.println(woman.getName()+"，"+action+"时，眼泪汪汪，谁也劝不好");
	}

}
