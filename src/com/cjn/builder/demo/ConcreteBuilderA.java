package com.cjn.builder.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午10:19:46
*@remark  具体建造者类
*
**/
public class ConcreteBuilderA extends Builder {
	
	private Product productA = new Product();

	@Override
	public void buildPartA() {
		// TODO Auto-generated method stub
		productA.addPart("部件A");
	}

	@Override
	public void buildPartB() {
		// TODO Auto-generated method stub
		productA.addPart("部件B");
	}

	@Override
	public Product getResult() {
		// TODO Auto-generated method stub
		return productA;
	}

}
