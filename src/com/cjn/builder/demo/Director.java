package com.cjn.builder.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午10:22:41
*@remark 指挥者类
*
**/
public class Director {
	
	public void construct(Builder builder){
		builder.buildPartA();
		builder.buildPartB();
	}
	
}
