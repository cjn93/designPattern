package com.cjn.builder.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午10:14:51
*@remark 抽象建造者类  确定产品由A、B两部分组成，并声明一个得到产品建造后的结果方法
*
**/
public abstract class Builder {
	
	public abstract void buildPartA();
	
	public abstract void buildPartB();
	
	public abstract Product getResult();

}
