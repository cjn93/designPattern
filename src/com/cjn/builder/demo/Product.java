package com.cjn.builder.demo;

import java.util.*;


/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午10:09:25
*@remark  产品类，由多个部件组成
*
**/
public class Product {
	
	
	List<String> parts = new ArrayList<>();
	
	public void addPart(String part){
		parts.add(part);
	}
	
	public void showParts(){
		System.out.println("产品列表：");
		for (String str : parts) {
			System.out.println(str);
		}
	}
	

}
