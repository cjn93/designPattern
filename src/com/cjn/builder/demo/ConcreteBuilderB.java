package com.cjn.builder.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午10:19:46
*@remark  具体建造者类
*
**/
public class ConcreteBuilderB extends Builder {
	
	private Product productB = new Product();

	@Override
	public void buildPartA() {
		// TODO Auto-generated method stub
		productB.addPart("部件X");
	}

	@Override
	public void buildPartB() {
		// TODO Auto-generated method stub
		productB.addPart("部件Y");
	}

	@Override
	public Product getResult() {
		// TODO Auto-generated method stub
		return productB;
	}

}
