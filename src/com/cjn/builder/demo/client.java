package com.cjn.builder.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午10:24:21
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		
		Director director = new Director();
		Builder bA = new ConcreteBuilderA();
		Builder bB = new ConcreteBuilderB();
		
		director.construct(bA);
		Product product = bA.getResult();
		product.showParts();
		
		director.construct(bB);
		product = bB.getResult();
		product.showParts();
		
		
		
		
		
	}
	
}
