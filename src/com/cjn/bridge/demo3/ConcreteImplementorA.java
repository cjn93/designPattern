package com.cjn.bridge.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午4:45:35
*@remark 
*
**/
public class ConcreteImplementorA extends Implementor {

	@Override
	public void operation() {
		// TODO Auto-generated method stub
		System.out.println("具体实现A的方法执行");
	}

}
