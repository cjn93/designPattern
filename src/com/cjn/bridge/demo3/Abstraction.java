package com.cjn.bridge.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午4:47:20
*@remark 
*
**/
public  class Abstraction {
	
	protected Implementor implementor;
	
	public void setImplementor(Implementor implementor){
		this.implementor = implementor;
	}
	
	public  void run(){
		implementor.operation();
	};
	
	

}
