package com.cjn.bridge.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午4:52:48
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Abstraction ab = new RefinedAbstraction();
		
		ab.setImplementor(new ConcreteImplementorA());
		ab.run();
		
		ab.setImplementor(new ConcreteImplementorB());
		ab.run();
		
		
	}

}
