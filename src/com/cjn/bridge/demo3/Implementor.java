package com.cjn.bridge.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午4:44:46
*@remark 
*
**/
public abstract class Implementor {
	
	public abstract void operation();

}
