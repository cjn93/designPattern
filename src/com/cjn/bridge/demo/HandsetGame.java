package com.cjn.bridge.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 上午10:56:26
*@remark 
*
**/
public abstract class HandsetGame {
	
	public abstract void run();

}
