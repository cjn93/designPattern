package com.cjn.bridge.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午1:57:13
*@remark 手机品牌类
*
**/
public abstract class HandsetBrand {
	
	public abstract void run();

}
