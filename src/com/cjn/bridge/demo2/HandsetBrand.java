package com.cjn.bridge.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午2:10:26
*@remark  手机品牌类
*
**/
public abstract class HandsetBrand {
	
	protected HandsetSoft hs;
	
	public void setHandsetSoft(HandsetSoft hs){
		this.hs = hs;
	}
	
	public abstract void run();
	

}
