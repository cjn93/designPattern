package com.cjn.bridge.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午2:25:44
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		HandsetBrand hb;
		
		hb = new HandsetBrandN();
		
		hb.setHandsetSoft(new HandsetGame());
		hb.run();
		hb.setHandsetSoft(new HandsetAddress());
		hb.run();
		
	}
	
}
