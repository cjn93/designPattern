package com.cjn.bridge.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午2:06:04
*@remark  手机软件
*
**/
public abstract class HandsetSoft {
	public abstract void run();
}
