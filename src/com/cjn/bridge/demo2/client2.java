package com.cjn.bridge.demo2;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午3:29:10
*@remark 
*
**/
public class client2 {
	//f8cd7dca3484b017e998c354445009b6
	public static void main(String[] args) {
		String str ="cjn";
		// TODO Auto-generated method stub
		MessageDigest md;   
		try{   
		    // 生成一个MD5加密计算摘要   
		    md = MessageDigest.getInstance("MD5");   
		    // 计算md5函数   
		    md.update(str.getBytes());
		    // digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符   
		    // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值   
		    String pwd = new BigInteger(1, md.digest()).toString(16);   
		   System.out.println(pwd);
		   } catch (Exception e) {
				System.err.println("md5转换错误"+e);
		   }
	}
	

}
