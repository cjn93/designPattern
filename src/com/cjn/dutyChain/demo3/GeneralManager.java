package com.cjn.dutyChain.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午5:47:06
*@remark 
*
**/
public class GeneralManager extends Manager {

	public GeneralManager(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void requestApplation(Request request) {
		// TODO Auto-generated method stub
		if("请假".equals(request.getRequestType())){
			System.out.println(super.getName()+":批准"+request.getName()+"，请假"+request.getNumber()+"天");
		}else if("加薪".equals(request.getRequestType()) && request.getNumber() <= 500){
			System.out.println(super.getName()+":批准"+request.getName()+"，加薪"+request.getNumber()+"元");
		}else if("加薪".equals(request.getRequestType()) && request.getNumber() >= 500){
			System.out.println(super.getName()+":"+request.getName()+"加薪"+request.getNumber()+"元，再说吧");
		}
		
	}

}
