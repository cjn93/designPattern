package com.cjn.dutyChain.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午4:07:20
*@remark 
*
**/
public abstract class Manager {
	
	private String name;
	//管理者的上级
	protected Manager  superior;
	
	public Manager(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	//set and get
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Manager getSuperior() {
		return superior;
	}
	//设置管理者的上级
	public void setSuperior(Manager superior) {
		this.superior = superior;
	}
	
	//
	public abstract void requestApplation(Request request);

}
