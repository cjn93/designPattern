package com.cjn.dutyChain.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午4:03:25
*@remark 
*
**/
public class Request {
	
	//name
	private String name;
	//申请类别
	private String requestType;
	//申请内容
	private String requestContent;
	//数量
	private int number;
	
	public Request(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	//set and get
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getRequestContent() {
		return requestContent;
	}
	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
	
}
