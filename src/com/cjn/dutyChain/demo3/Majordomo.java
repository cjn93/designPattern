package com.cjn.dutyChain.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午5:47:06
*@remark 
*
**/
public class Majordomo extends Manager {

	public Majordomo(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void requestApplation(Request request) {
		// TODO Auto-generated method stub
		if("请假".equals(request.getRequestType()) && request.getNumber() <= 5){
			System.out.println(super.getName()+":批准"+request.getName()+"，请假"+request.getNumber()+"天");
		}else{
			if(super.superior != null){
				super.superior.requestApplation(request);
			}
		}
		
	}

}
