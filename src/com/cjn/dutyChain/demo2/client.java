package com.cjn.dutyChain.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午5:24:21
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Handler h1 = new ConcreteHandler();
		Handler h2 = new ConcreteHandler2();
		Handler h3 = new ConcreteHandler3();
		
		h1.setHandler(h2);
		h2.setHandler(h3);
		
		int[] requests = {2,5,14,17,8,20,21,11,25};
		
		for (int i : requests) {
			h1.handlerRequest(i);
		}
		
		
		
	}

}
