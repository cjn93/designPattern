package com.cjn.dutyChain.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午5:18:30
*@remark 
*
**/
public class ConcreteHandler2 extends Handler {

	@Override
	public void handlerRequest(int request) {
		// TODO Auto-generated method stub
		if(request >= 10 && request < 20){
			System.out.println(this.getClass().getName()+"处理请求："+request);
		}else if(super.handler != null){
			super.handler.handlerRequest(request);
		}
	}

}
