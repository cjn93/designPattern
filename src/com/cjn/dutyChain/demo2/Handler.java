package com.cjn.dutyChain.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午4:52:26
*@remark  定义一个处理请示的接口
*
**/
public abstract class Handler {
	
	protected Handler handler;

	

	public void setHandler(Handler handler) {
		this.handler = handler;
	}
	
	public abstract void handlerRequest(int request);	
	

}
