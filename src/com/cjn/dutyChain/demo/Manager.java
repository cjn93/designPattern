package com.cjn.dutyChain.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午4:07:20
*@remark 
*
**/
public class Manager {
	
	private String name;
	
	public Manager(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	public void getResult(String managerLevel,Request request){
		
		if("经理".equals(managerLevel)){
			if("请假".equals(request.getRequestType()) && request.getNumber() <= 2){
				System.out.println("批准，请假"+request.getNumber()+"天");
			}else{
				System.out.println("无权批准，请假"+request.getNumber()+"天");
			}
		}else if("总监".equals(managerLevel)){
			if("请假".equals(request.getRequestType()) && request.getNumber() <= 5){
				System.out.println("批准，请假"+request.getNumber()+"天");
			}else{
				System.out.println("无权批准，请假"+request.getNumber()+"天");
			}
		}else if("总经理".equals(managerLevel)){
			if("请假".equals(request.getRequestType())){
				System.out.println("批准，请假"+request.getNumber()+"天");
			}else if("加薪".equals(request.getRequestType()) && request.getNumber() <= 500){
				System.out.println("批准，加薪"+request.getNumber()+"元");
			}else if("加薪".equals(request.getRequestType()) && request.getNumber() >= 500){
				System.out.println("加薪"+request.getNumber()+"元，再说吧");
			}
		}
		
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
