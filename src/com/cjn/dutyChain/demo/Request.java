package com.cjn.dutyChain.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午4:03:25
*@remark 
*
**/
public class Request {
	
	//申请类别
	private String requestType;
	//申请内容
	private String requestContent;
	//数量
	private int number;
	
	//set and get
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getRequestContent() {
		return requestContent;
	}
	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
	
}
