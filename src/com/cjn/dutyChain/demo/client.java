package com.cjn.dutyChain.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午4:19:38
*@remark 
*
**/
public class client {
  public static void main(String[] args) {
	  	Manager jingli = new Manager("景丽");
	  	Manager zongjian = new Manager("宗建");
	  	Manager zongjingli = new Manager("宗景丽");
	  
	  	Request r1 = new Request();
	  	
	  	r1.setRequestType("请假");
	  	r1.setRequestContent("小菜请假");
	  	r1.setNumber(4);
	  	
	  	jingli.getResult("经理", r1);
	  	zongjian.getResult("总监", r1);
	  	zongjingli.getResult("总经理", r1);
	  	
	  	r1.setRequestType("加薪");
	  	r1.setRequestContent("小菜加薪");
	  	r1.setNumber(1000);
	  	
	  	zongjingli.getResult("总经理", r1);
	  	
  }
}
