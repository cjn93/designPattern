package com.cjn.command.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午5:46:01
*@remark 
*
**/
public class BakeMuttonCommand extends Command {
	
	String str ="羊肉串";

	public BakeMuttonCommand(Barbecuer receiver) {
		super(receiver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void excuteCommand() {
		// TODO Auto-generated method stub
		receiver.bakeMutton();
	}

	@Override
	public String toString() {
		return  str;
	}
	

}
