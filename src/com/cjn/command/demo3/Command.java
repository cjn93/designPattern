package com.cjn.command.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午5:41:28
*@remark 
*
**/
public abstract class Command {

	protected Barbecuer receiver;
	
	public Command(Barbecuer receiver){
		this.receiver = receiver;
	} 
	
	public abstract void excuteCommand();
	
	
	
}
