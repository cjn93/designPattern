package com.cjn.command.demo3;

import java.util.*;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午5:50:59
*@remark 
*
**/
public class Waiter {
	
	//private Command command;
	private List<Command> list = new ArrayList<>();
	
	//设置订单
	public void setOrder(Command command){
		if("鸡翅".equals(command.toString())){
			//System.out.println(command.toString());
			System.out.println("服务员：鸡翅没有了,请点别的烧烤！");
		}else{
			list.add(command);
			System.out.println("服务员：增加订单："+command.toString());
			System.out.println("时间："+new Date());
		}
	}
	//取消订单
	public void cancelOrder(Command command){
		
		list.remove(command);
		System.out.println("服务员：取消订单："+command.toString());
		System.out.println("时间："+new Date());
	}
	
	
	//通知操作
	public void notifyInfo(){
		
		for (Command command : list) {
			command.excuteCommand();
		}
		
	}
	
	
	

}
