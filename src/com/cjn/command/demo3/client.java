package com.cjn.command.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午5:56:14
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Barbecuer boy = new Barbecuer();
		
		Command com1= new BakeMuttonCommand(boy);
		Command com2= new BakeMuttonCommand(boy);
		Command com3= new BakeMuttonCommand(boy);
		Command com4= new BakeChickenCommand(boy);
		
		Waiter girl = new Waiter();
		
		girl.setOrder(com1);
		girl.setOrder(com2);
		girl.setOrder(com3);
		girl.setOrder(com4);
		
		
		girl.notifyInfo();
		
		
	}

}
