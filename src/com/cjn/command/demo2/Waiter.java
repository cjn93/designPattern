package com.cjn.command.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午5:50:59
*@remark 
*
**/
public class Waiter {
	
	private Command command;
	
	//设置订单
	public void setOrder(Command command){
		this.command = command;
	}
	
	//通知操作
	public void notifyInfo(){
		command.excuteCommand();
	}
	
	
	

}
