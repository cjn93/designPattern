package com.cjn.command.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午5:47:54
*@remark 
*
**/
public class BakeChickenCommand  extends Command{

	public BakeChickenCommand(Barbecuer receiver) {
		super(receiver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void excuteCommand() {
		// TODO Auto-generated method stub
		receiver.bakeChicken();
	}

}
