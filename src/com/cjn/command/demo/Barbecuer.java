package com.cjn.command.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月19日 下午5:19:03
*@remark 
*
**/
public class Barbecuer {
	
	public void bakeMutton(){
		System.out.println("烤羊肉串！！！");
	}
	
	public void bakeChicken(){
		System.out.println("烤鸡翅！！！");
	}

}
