package com.cjn.command.demo4;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午3:35:03
*@remark 
*
**/
public class client {
   
	public static void main(String[] args) {
		Receiver receiver = new Receiver();
		Command command = new ConcreteCommand(receiver);
		Invoker invoker = new Invoker();
		
		invoker.setCommand(command);
		invoker.excuteCommand();
		
		
		
	}
}
