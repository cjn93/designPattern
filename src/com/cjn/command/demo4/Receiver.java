package com.cjn.command.demo4;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午3:23:20
*@remark  知道如何实施与执行与一个请求相关的操作，任何类都可以成为一个接收者
*
**/
public class Receiver {

	public void action() {
		// TODO Auto-generated method stub
		System.out.println("执行操作");
	}

}
