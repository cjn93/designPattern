package com.cjn.command.demo4;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午3:33:02
*@remark 要求该命令执行这个请求
*
**/
public class Invoker {
	
	private Command command;
	
	public Command getCommand() {
		return command;
	}

	public void setCommand(Command command) {
		this.command = command;
	}
	
	
	public void excuteCommand(){
		command.excute();
	}
	
	

}
