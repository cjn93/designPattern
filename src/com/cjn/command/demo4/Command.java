package com.cjn.command.demo4;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午3:21:34
*@remark  用来声明执行操作的接口
*
**/
public abstract class Command {
	
	protected Receiver receiver;
	
	public Command(Receiver receiver) {
		// TODO Auto-generated constructor stub
		this.receiver = receiver;
	}
	
	public abstract void excute();

}
