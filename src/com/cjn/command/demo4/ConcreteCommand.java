package com.cjn.command.demo4;
/**
*@author cjn
*@version 1.0
*@date 2018年6月20日 下午3:26:50
*@remark  将一个接收者对象绑定一个动作，调用接收者相应的操作，以实现excute方法
*
**/
public class ConcreteCommand extends Command {

	public ConcreteCommand(Receiver receiver) {
		super(receiver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void excute() {
		// TODO Auto-generated method stub
		super.receiver.action();
	}

}
