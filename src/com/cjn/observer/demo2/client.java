package com.cjn.observer.demo2;

/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午1:28:14
*@remark 
*
**/
public class client {
	
	
	  public static void main(String[] args) {
		   Secretary qian = new Secretary();
			
			Observer s1 = new StockObserver("cjn_1", qian);
			Observer s2 = new NBAObserver("cjn_2", qian);
			
			qian.attach(s1);
			qian.attach(s2);
			
			qian.setSubjectStates("老板来了");
			
			qian.notifyStockObserver();
	 }
}
