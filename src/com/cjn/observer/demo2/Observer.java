package com.cjn.observer.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午1:18:17
*@remark 
*
**/
public abstract class Observer {
	
	protected String name;
	protected Subjects sub;
	
	public Observer( String name,Subjects sub) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.sub = sub;
	}
	
	public abstract void update();
	

}
