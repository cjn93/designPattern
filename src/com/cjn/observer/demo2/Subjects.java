package com.cjn.observer.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午1:48:15
*@remark 
*
**/
public abstract class Subjects {
	
	private String subjectStates;
	
	public abstract void attach(Observer observer);
	
	public abstract void detach(Observer observer);
	
	public abstract void notifyStockObserver();

	public String getSubjectStates() {
		return subjectStates;
	}

	public void setSubjectStates(String subjectStates) {
		this.subjectStates = subjectStates;
	}
	
}
