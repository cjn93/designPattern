package com.cjn.observer.demo2;

import java.util.ArrayList;
import java.util.List;

/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午11:18:23
*@remark 
*
**/
public class Secretary extends Subjects {
	
	//同事列表
	List<Observer> list = new ArrayList<Observer>();
	
	private String action;
	
	@Override
	public void attach(Observer observer){
		list.add(observer);
	}
	
	@Override
	public void notifyStockObserver(){
		for (Observer observer : list) {
			observer.update();
		}
	}
	@Override
	public void detach(Observer observer) {
		// TODO Auto-generated method stub
		
	}

	
	//set and get
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
