package com.cjn.observer.demo1;

import java.util.ArrayList;
import java.util.List;

/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午11:18:23
*@remark 
*
**/
public class Secretary {
	
	//同事列表
	List<Observer> list = new ArrayList<Observer>();
	
	private String action;
	
	public void attach(Observer observer){
		list.add(observer);
	}
	
	public void notifyStockObserver(){
		for (Observer observer : list) {
			observer.update();
		}
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	
	

}
