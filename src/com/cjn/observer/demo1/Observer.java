package com.cjn.observer.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午1:18:17
*@remark 
*
**/
public abstract class Observer {
	
	protected String name;
	protected Secretary sec;
	
	public Observer( String name,Secretary sec) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.sec = sec;
	}
	
	public abstract void update();
	

}
