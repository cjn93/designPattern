package com.cjn.observer.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午11:17:18
*@remark 
*
**/
public class NBAObserver extends Observer {

	public NBAObserver(String name, Secretary sec) {
		super(name, sec);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		System.out.println(sec.getAction()+","+this.name+"，关闭NBA，赶紧工作！！");
	}
	
	

}
