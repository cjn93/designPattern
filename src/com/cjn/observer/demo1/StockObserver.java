package com.cjn.observer.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午11:17:18
*@remark 
*
**/
public class StockObserver extends Observer {

	public StockObserver(String name, Secretary sec) {
		super(name, sec);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		System.out.println(sec.getAction()+","+name+"，关闭股市，赶紧工作！！");
	}
	
	

}
