package com.cjn.observer.demo;

import java.util.ArrayList;
import java.util.List;

/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午11:18:23
*@remark 
*
**/
public class Secretary {
	
	//同事列表
	List<StockObserver> list = new ArrayList<>();
	
	private String action;
	
	public void attach(StockObserver observer){
		list.add(observer);
	}
	
	public void notifyStockObserver(){
		for (StockObserver stockObserver : list) {
			stockObserver.update();
		}
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	
	

}
