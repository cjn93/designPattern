package com.cjn.observer.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午11:17:18
*@remark 
*
**/
public class StockObserver {
	
	private String name;
	private Secretary sec;
	
	public StockObserver(String name,Secretary sec) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.sec = sec;
	}
	
	public void update(){
		System.out.println(sec.getAction()+","+name+"，关闭股市，赶紧工作！！");
	}

}
