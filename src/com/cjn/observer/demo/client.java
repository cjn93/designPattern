package com.cjn.observer.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 上午11:31:50
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		Secretary qian = new Secretary();
		
		StockObserver s1 = new StockObserver("cjn_1", qian);
		StockObserver s2 = new StockObserver("cjn_2", qian);
		
		qian.attach(s1);
		qian.attach(s2);
		
		qian.setAction("老板来了");
		
		qian.notifyStockObserver();
		
	}

}
