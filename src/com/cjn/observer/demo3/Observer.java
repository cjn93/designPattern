package com.cjn.observer.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午3:39:52
*@remark  抽象观察者
*
**/
public abstract class Observer {

	public abstract void update() ;

}
