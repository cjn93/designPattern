package com.cjn.observer.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午3:53:53
*@remark 
*
**/
public class client {
	
		public static void main(String[] args) {
			
			ConcreteSubject s = new ConcreteSubject();
			
			s.addtach(new ConcreteObserver("X", s));
			s.addtach(new ConcreteObserver("Y", s));
			s.addtach(new ConcreteObserver("Z", s));
			
			s.setSubjectStates("ABC");
			
			s.notifyObserver();
			
		}

}
