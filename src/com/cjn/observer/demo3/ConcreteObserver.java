package com.cjn.observer.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午3:48:36
*@remark  具体观察者
*
**/
public class ConcreteObserver extends Observer {
	
	private String name;
	private String observerStates;
	private ConcreteSubject subject;
	
	public ConcreteObserver(String name,ConcreteSubject subject) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.subject = subject;
	}
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		observerStates = subject.getSubjectStates();
		System.out.println("观察者:"+name+"的新状态是:"+observerStates);
		
	}
	
	
	//set and get
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getObserverStates() {
		return observerStates;
	}

	public void setObserverStates(String observerStates) {
		this.observerStates = observerStates;
	}

	public ConcreteSubject getSubject() {
		return subject;
	}

	public void setSubject(ConcreteSubject subject) {
		this.subject = subject;
	}
	
	

}
