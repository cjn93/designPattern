package com.cjn.observer.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午3:43:54
*@remark 
*
**/
public class ConcreteSubject extends Subject {
	
	private String subjectStates;

	
	//set and get
	public String getSubjectStates() {
		return subjectStates;
	}

	public void setSubjectStates(String subjectStates) {
		this.subjectStates = subjectStates;
	}
	
	

}
