package com.cjn.observer.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午2:44:23
*@remark  抽象通知者
*
**/

import java.util.*;

public abstract class Subject {
	
	private List<Observer> list = new ArrayList<>();
	
	//增加观察者
	public  void addtach(Observer observer){
		list.add(observer);
	};
	//删除观察者
	public  void deltach(Observer observer){
		list.remove(observer);
	};
	//通知
	public  void notifyObserver(){
		for (Observer observer : list) {
			observer.update();
		}
	}
	
	
}
