package com.cjn.flyweight.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:24:45
*@remark 一个享元工厂，用来创建并管理flyweight对象，他主要是用来确保合理的共享flyweight，
*		当用户请求一个flyweight时，flyweightfactory对象提供一个已创建的实例或者创建一个(如果不存在的话)
*
**/

import java.util.*;

public class FlyweightFactory {
	
	private Hashtable<String,Object> flytables = new Hashtable<>();
	
	public FlyweightFactory() {
		// TODO Auto-generated constructor stub
		flytables.put("X", new ConcreteFlyweight());
		flytables.put("Y", new ConcreteFlyweight());
		flytables.put("Z", new ConcreteFlyweight());
	}
	
	public Flyweight getFlyWeight(String key){
		return (Flyweight) flytables.get(key);
	}
	
	

}
