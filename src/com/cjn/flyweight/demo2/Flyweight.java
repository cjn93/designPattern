package com.cjn.flyweight.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:14:50
*@remark 所有具体享元类的超类或者接口，通过这个接口，可以接受并作用于外部状态
*
**/
public abstract class Flyweight {
	
	public abstract void operation(int extrinsicstate);

}
