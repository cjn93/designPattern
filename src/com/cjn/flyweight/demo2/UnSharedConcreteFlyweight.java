package com.cjn.flyweight.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:20:19
*@remark 指那些不需要共享的flyweight子类，因为flyweight接口共享成为可能，但他并不强制共享
*
**/
public class UnSharedConcreteFlyweight extends Flyweight {

	@Override
	public void operation(int extrinsicstate) {
		// TODO Auto-generated method stub
		System.out.println("不共享具体的flyweight："+extrinsicstate);
	}

}
