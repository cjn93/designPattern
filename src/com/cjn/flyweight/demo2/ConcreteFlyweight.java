package com.cjn.flyweight.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:20:19
*@remark 继承flyweight超类或者实现flyweight接口。并为内部状态增加存储空间
*
**/
public class ConcreteFlyweight extends Flyweight {

	@Override
	public void operation(int extrinsicstate) {
		// TODO Auto-generated method stub
		System.out.println("具体flyweight："+extrinsicstate);
	}

}
