package com.cjn.flyweight.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:31:54
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 22;	
		
		FlyweightFactory ff = new FlyweightFactory();
		
		Flyweight fx = ff.getFlyWeight("X");
		fx.operation(--num);

		Flyweight fy = ff.getFlyWeight("Y");
		fy.operation(--num);
		
		Flyweight fz = ff.getFlyWeight("Z");
		fz.operation(--num);
		
		UnSharedConcreteFlyweight uf = new UnSharedConcreteFlyweight();
		uf.operation(--num);
		
		
		
	}

}
