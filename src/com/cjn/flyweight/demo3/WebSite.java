package com.cjn.flyweight.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:48:43
*@remark 
*
**/
public abstract class WebSite {
	
	public abstract void use();
	
}
