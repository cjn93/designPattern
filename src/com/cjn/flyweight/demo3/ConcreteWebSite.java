package com.cjn.flyweight.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:49:59
*@remark 
*
**/
public class ConcreteWebSite extends WebSite {
	
	private String name;
	
	public ConcreteWebSite(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}

	@Override
	public void use() {
		// TODO Auto-generated method stub
		System.out.println("网站分类："+name);
	}

}
