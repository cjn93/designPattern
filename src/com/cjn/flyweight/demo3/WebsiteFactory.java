package com.cjn.flyweight.demo3;

import java.util.*;

/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午10:54:11
*@remark 
*
**/
public class WebsiteFactory {
	
	private Map<String,Object> maps = new HashMap<>();
	
	public WebSite getWebSite(String key){
		if(maps.get(key) == null){
			maps.put(key, new ConcreteWebSite(key));
		}
		return (WebSite) maps.get(key);
	}
	
	public int websiteCount(){
		return maps.size();
	}

}
