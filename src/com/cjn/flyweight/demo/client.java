package com.cjn.flyweight.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午9:59:23
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebSite fx = new WebSite("产品展示");
		fx.use();
		
		WebSite fy = new WebSite("博客");
		fy.use();
		
	}

}
