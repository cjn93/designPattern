package com.cjn.flyweight.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午5:20:53
*@remark 
*
**/
public class WebSite {
	
	private String name;
	
	public WebSite(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	public void use(){
		System.out.println("网站分类："+name);
	}
	

}
