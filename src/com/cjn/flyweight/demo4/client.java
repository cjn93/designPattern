package com.cjn.flyweight.demo4;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 上午11:08:14
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebsiteFactory f = new WebsiteFactory();
		
		WebSite fx = f.getWebSite("博客");
		fx.use(new User("小菜"));
		
		WebSite fy = f.getWebSite("产品展示");
		fy.use(new User("大鸟"));
		
		WebSite fz = f.getWebSite("博客");
		fz.use(new User("娇娇"));
		
		WebSite fa = f.getWebSite("产品展示");
		fa.use(new User("老顽童"));
		
		System.out.println("************");
		System.out.println("网站种类："+f.websiteCount()+"种");
		System.out.println("************");
		
		
	}

}
