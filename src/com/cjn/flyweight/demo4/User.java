package com.cjn.flyweight.demo4;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午1:05:21
*@remark 
*
**/
public class User {
	
	private String name;
	
	public User(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
