package com.cjn.facade.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午2:04:17
*@remark 
*
**/
public class client {
	public static void main(String[] args) {
		 Facade facade = new Facade();
		 
		 facade.methodA();
		 facade.methodB();
		 
		 
	}
}	
