package com.cjn.facade.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:59:04
*@remark 外观类，知道哪些子系统负责处理请求，将客户的请求代理给适当的子系统对象
*
**/
public class Facade {
	
	private SubSystemOne one;
	private SubSystemTwo two;
	private SubSystemThree three;
	private SubSystemFour four;

	public Facade() {
		// TODO Auto-generated constructor stub
		  one = new SubSystemOne();
		  two = new SubSystemTwo();
		  three = new SubSystemThree();
		  four = new SubSystemFour();
	}
	
	
	public void methodA(){
		System.out.println("方法组A");
		one.methodOne();
		three.methodThree();
	}
	
	public void methodB(){
		System.out.println("方法组B");
		two.methodTwo();
		four.methodFour();
	}
	
}
