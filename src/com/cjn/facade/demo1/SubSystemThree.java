package com.cjn.facade.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:56:04
*@remark 
*
**/
public class SubSystemThree {
	
	public void methodThree(){
		System.out.println("子系统方法三");
	}

}
