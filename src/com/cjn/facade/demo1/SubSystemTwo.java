package com.cjn.facade.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:56:04
*@remark 
*
**/
public class SubSystemTwo {
	
	public void methodTwo(){
		System.out.println("子系统方法二");
	}

}
