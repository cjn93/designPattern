package com.cjn.facade.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:43:52
*@remark 基金类
*
**/
public class Fund {
	
	private Stock1 s1;
	private Stock2 s2;
	private NationalDebt1 nd1;
	private Realty1 r1 ;
	
	public Fund() {
		// TODO Auto-generated constructor stub
		 s1 = new Stock1();
		 s2 = new Stock2();
		 nd1 = new NationalDebt1();
		 r1 = new Realty1();
	}
	
	public void sell(){
		s1.sell();
		s2.sell();
		nd1.sell();
		r1.sell();
	}
	public void buy(){
		s1.buy();
		s2.buy();
		nd1.buy();
		r1.buy();
	}
	

}
