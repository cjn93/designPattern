package com.cjn.facade.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:24:43
*@remark 
*
**/
public class Stock2 {
	
	public void sell(){
		System.out.println("卖出 Stock2");
	}
	public void buy(){
		
		System.out.println("买入 Stock2");
		
	}
}
