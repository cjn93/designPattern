package com.cjn.facade.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:31:52
*@remark  客户端1
*
**/
public class client {
	public static void main(String[] args) {
		Stock1 s1 = new Stock1();
		Stock2 s2 = new Stock2();
		NationalDebt1 nd1 = new NationalDebt1();
		Realty1 r1 = new Realty1();
		
		s1.buy();
		s2.buy();
		nd1.buy();
		r1.buy();
		
		System.out.println("*****");
		
		s1.sell();
		s2.sell();
		nd1.sell();
		r1.sell();
		
		
	}
}
