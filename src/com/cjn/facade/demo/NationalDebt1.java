package com.cjn.facade.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:28:09
*@remark 
*
**/
public class NationalDebt1 {
	
	public void sell(){
		System.out.println("卖出 NationalDebt1");
	}
	
	public void buy(){
		
		System.out.println("买入 NationalDebt1");
	}
}
