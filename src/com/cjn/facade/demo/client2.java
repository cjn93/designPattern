package com.cjn.facade.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:46:24
*@remark  客户端2 通过基金来实现
*
**/
public class client2 {
	public static void main(String[] args) {
		Fund jijin = new Fund();
		
		jijin.buy();
		System.out.println("****");
		jijin.sell();
		
	}
}
