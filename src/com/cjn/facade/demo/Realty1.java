package com.cjn.facade.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月30日 下午1:31:03
*@remark 
*
**/
public class Realty1 {
	
	public void sell(){
		System.out.println("卖出 Realty1");
	}
	
	public void buy(){
		
		System.out.println("买入 Realty1");
	}

}
