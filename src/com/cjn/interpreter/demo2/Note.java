package com.cjn.interpreter.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午2:03:56
*@remark 非终结符表达式，为文法中的非终结符实现解释操作。
*
**/
public class Note extends Expression {

	@Override
	public void excute(String key, double value) {
		// TODO Auto-generated method stub
		String note = "";
		switch (key) {
		case "C":
			note = "1";
			break;
		case "D":
			note = "2";	
			break;
		case "E":
			note = "3";
			break;
		case "F":
			note = "4";
			break;
		case "G":
			note = "5";
			break;
		case "A":
			note = "6";
			break;
		case "B":
			note = "7";
			break;
		}
		//System.out.println("note:"+note);
		System.out.print(note+" ");
		
	}

	
}
