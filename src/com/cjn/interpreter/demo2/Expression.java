package com.cjn.interpreter.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午2:00:16
*@remark 
*
**/
public abstract class Expression {
	
	public  void interpreter(PlayContext context){
		if(context.getText().length() == 0){
			return ;
		}else{
			
			String playKey = context.getText().substring(0,1);
			context.setText(context.getText().substring(2));
			
			double playValue = Double.parseDouble(context.getText().substring(0,context.getText().indexOf(" ")));
			context.setText(context.getText().substring(context.getText().indexOf(" ")+1));
			
			excute(playKey, playValue);
			
		}
	};
	
	public abstract void excute(String key,double value);
	
	
}
