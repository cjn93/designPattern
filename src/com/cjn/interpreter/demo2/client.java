package com.cjn.interpreter.demo2;


/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午2:08:58
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PlayContext context = new PlayContext();
		String str ="O 2 E 0.5 G 0.5 A 3 E 0.5 G 0.5 D 3 E 0.5 G 0.5 A 0.5 O 3 C 1 O 2 A 0.5 G 1 C 0.5 E 0.5 D 3 ";
		//第一位不能为空 也不能为" " 书中示例有误
		context.setText(str);
		
		Expression expression = null;
		
		
		try{
			while(context.getText().length() > 0){
				//System.out.println("length:"+context.getText().length());
				String s = context.getText().substring(0,1);
				
				switch (s) {
				case "O":
					expression = new Scale();
					break;
				case "C":
				case "D":
				case "E":
				case "F":
				case "G":
				case "A":
				case "B":
				case "P":
					expression = new Note();
					break;
				}
				int num = 0;
				if(expression == null){
					System.out.println(num);
					num++;
				}else{
					expression.interpreter(context);
				}
				
			}//while end
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("异常"+e.getMessage());
			
		}
		
		
		
		
		
	}

}
