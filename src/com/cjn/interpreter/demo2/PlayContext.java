package com.cjn.interpreter.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午2:01:45
*@remark  包含解释器之外的一些全局信息
*
**/
public class PlayContext {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
	
}
