package com.cjn.interpreter.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午2:03:56
*@remark 终结符表达式，实现与文法中的终结符相关联的解释操作。实现抽象表法师中所要求的接口
*
**/
public class Scale extends Expression {

	@Override
	public void excute(String key, double value) {
		// TODO Auto-generated method stub
		String sclae = "";
		switch ((int)value) {
		case 1:
			sclae = "低音";
			break;
		case 2:
			sclae = "中音";
			break;
		case 3:
			sclae = "高音";
			break;
		}
		//System.out.println("scale："+sclae);
		
		System.out.print(sclae+" ");
	}



}
