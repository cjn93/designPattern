package com.cjn.interpreter.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午2:00:16
*@remark 抽象表达式 声明一个抽象的解释操作，这个接口为抽象语法树中所有的节点所共享。
*
**/
public abstract class AbstractExpression {
	
	public abstract void interpreter(Context context);
	
}
