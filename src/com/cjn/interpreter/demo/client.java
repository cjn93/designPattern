package com.cjn.interpreter.demo;

import java.util.*;

/**
*@author cjn
*@version 1.0
*@date 2018年6月22日 下午2:08:58
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Context context = new Context();
		List<AbstractExpression> list = new ArrayList<>();
		list.add(new TerminalExpression());
		list.add(new TerminalExpression());
		list.add(new NonterminalExpression());
		list.add(new TerminalExpression());
		list.add(new TerminalExpression());
		
		
		for (AbstractExpression abstractExpression : list) {
			abstractExpression.interpreter(context);
		}
		
	}

}
