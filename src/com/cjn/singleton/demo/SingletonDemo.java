package com.cjn.singleton.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 下午3:18:16
*@remark  恶汉式单例模式
*
**/
public class SingletonDemo {
	
	private static SingletonDemo instance = new SingletonDemo();
	
	private SingletonDemo() {
		// TODO Auto-generated constructor stub
		
	}
	
	public static SingletonDemo getInstance(){
		return instance;
	}
	

}
