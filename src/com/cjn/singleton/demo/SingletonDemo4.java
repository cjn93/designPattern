package com.cjn.singleton.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 下午3:47:54
*@remark 
*
**/
public class SingletonDemo4 {
	
	 	private volatile static SingletonDemo4 instance;
	 	
	    private SingletonDemo4(){
	        System.out.println("Singleton has loaded");
	    }
	    
	    public static SingletonDemo4 getInstance(){
	        if(instance==null){
	            synchronized (SingletonDemo4.class){
	                if(instance==null){
	                    instance=new SingletonDemo4();
	                }
	            }
	        }
	        return instance;
	    }
}
