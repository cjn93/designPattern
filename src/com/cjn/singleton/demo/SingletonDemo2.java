package com.cjn.singleton.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 下午3:28:19
*@remark  饱汉式的单例模式  线程安全的
*
**/
public class SingletonDemo2 {
	
	private static SingletonDemo2 instance = null;
	
	private  SingletonDemo2() {
		// TODO Auto-generated constructor stub
	}
	
	public synchronized SingletonDemo2 getInstance(){
		if(instance == null ){
			instance = new SingletonDemo2();
		}
		return instance;
	}
}
