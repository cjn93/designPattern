package com.cjn.singleton.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 下午3:33:42
*@remark  用静态类加载的恶汉式单例模式
*
**/
public class SingletonDemo3 {
	
		private static class SingletonHolder{
	        private static SingletonDemo3 instance=new SingletonDemo3();
	    }
		
	    private SingletonDemo3(){
	    	
	        System.out.println("Singleton has loaded");
	    }
	    
	    public static SingletonDemo3 getInstance(){
	        return SingletonHolder.instance;
	    }
}
