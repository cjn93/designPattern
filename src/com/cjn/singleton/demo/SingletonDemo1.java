package com.cjn.singleton.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月5日 下午3:21:54
*@remark  饱汉式的单例模式
*	缺点：缺少线程安全的控制
*	
*
**/
public class SingletonDemo1 {
	
	private static SingletonDemo1 instance = null;
	
	private  SingletonDemo1() {
		// TODO Auto-generated constructor stub
	}
	
	public SingletonDemo1 getInstance(){
		if(instance == null ){
			instance = new SingletonDemo1();
		}
		return instance;
	}
	
	
	
}
