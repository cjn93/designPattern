package com.cjn.adapter.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午1:20:51
*@remark 
*
**/
public abstract class Player {
	
	protected String name;
	
	public Player(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	/**
	 * 进攻
	 * **/
	public abstract void attack();
	/**
	 * 防守
	 * **/
	public abstract void defense();
	

}
