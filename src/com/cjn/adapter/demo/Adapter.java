package com.cjn.adapter.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 上午10:30:18
*@remark 通过在内部包装一个adaptee对象，将源接口转换为目标接口
*
**/
public class Adapter extends Target {
	
	private Adaptee adaptee = new Adaptee();

	@Override
	public void request() {
		// TODO Auto-generated method stub
		adaptee.specialRequest();
	}
	
	
		
}
