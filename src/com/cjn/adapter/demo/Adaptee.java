package com.cjn.adapter.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 上午10:27:31
*@remark  需要适配的类
*
**/
public class Adaptee {

	public void specialRequest(){
		System.out.println("特殊的请求！");
	}
	
	
}
