package com.cjn.adapter.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 上午10:25:14
*@remark  这是客户所期待的接口，目标是可以是具体的类、抽象的类或者接口
*
**/
public class Target {
	
	public void request(){
		System.out.println("普通请求！");
	}

}
