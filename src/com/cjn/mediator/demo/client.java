package com.cjn.mediator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午2:43:57
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConcreteMediator m = new ConcreteMediator();
		
		ConcreteColleagueA a = new ConcreteColleagueA(m);
		ConcreteColleagueB b = new ConcreteColleagueB(m);
		
		m.setColleagueA(a);
		m.setColleagueB(b);
		
		a.send("吃饭了吗？");
		b.send("没有呢，你打算请客？");
		
		
		
		
	}

}
