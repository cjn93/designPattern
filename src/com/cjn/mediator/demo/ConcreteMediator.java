package com.cjn.mediator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午1:39:04
*@remark  具体中介者类
*
**/
public class ConcreteMediator extends Mediator {
	
	ConcreteColleagueA colleagueA;
	ConcreteColleagueB colleagueB;
	
	@Override
	public void send(String message, Colleague colleague) {
		// TODO Auto-generated method stub
		if(colleagueA == colleague){
			colleagueB.notifyInfo(message);
		}else{
			colleagueA.notifyInfo(message);
		}
	}
	
	
	//set and get
	public ConcreteColleagueA getColleagueA() {
		return colleagueA;
	}
	public void setColleagueA(ConcreteColleagueA colleagueA) {
		this.colleagueA = colleagueA;
	}
	public ConcreteColleagueB getColleagueB() {
		return colleagueB;
	}
	public void setColleagueB(ConcreteColleagueB colleagueB) {
		this.colleagueB = colleagueB;
	}

}
