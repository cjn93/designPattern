package com.cjn.mediator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 上午11:24:14
*@remark 抽象中介者类
*
**/
public abstract class Mediator {
	
	public abstract void send(String message,Colleague colleague);

}
