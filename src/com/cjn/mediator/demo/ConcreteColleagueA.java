package com.cjn.mediator.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午1:41:14
*@remark 
*
**/
public class ConcreteColleagueA extends Colleague{

	public ConcreteColleagueA(Mediator mediator) {
		super(mediator);
		// TODO Auto-generated constructor stub
	}
	
	public void notifyInfo(String message){
		System.out.println("同事A得到消息："+message);
	}
	
	public void send(String message){
		super.mediator.send(message, this);
	}
	
	

}
