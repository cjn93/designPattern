package com.cjn.mediator.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午1:39:04
*@remark  具体中介者类
*
**/
public class UniteNationsSecurityCouncil extends UniteNations {
	
	private USA usa;
	private Iraq iraq;

	@Override
	public void declare(String message, Country country) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				if(usa == country){
					iraq.notifyInfo(message);
				}else{
					usa.notifyInfo(message);
				}
	}
	
	//set and get
	public USA getUsa() {
		return usa;
	}
	public void setUsa(USA usa) {
		this.usa = usa;
	}
	public Iraq getIraq() {
		return iraq;
	}
	public void setIraq(Iraq iraq) {
		this.iraq = iraq;
	}
}
