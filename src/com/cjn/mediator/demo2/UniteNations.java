package com.cjn.mediator.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午3:12:06
*@remark  联合国机构
*
**/
public abstract class UniteNations {
	
	public abstract void declare(String message,Country country);

}
