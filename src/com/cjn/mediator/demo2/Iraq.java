package com.cjn.mediator.demo2;


/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午4:42:09
*@remark 
*
**/
public class Iraq extends Country {

	public Iraq(UniteNations uniteNations) {
		super(uniteNations);
		// TODO Auto-generated constructor stub
	}
	
	public void notifyInfo(String message){
		System.out.println("Iraq得到消息："+message);
	}
	
	public void send(String message){
		super.uniteNations.declare(message, this);
	}

}
