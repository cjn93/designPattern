package com.cjn.mediator.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月21日 下午4:49:08
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		UniteNationsSecurityCouncil unsc = new UniteNationsSecurityCouncil();
		
		
		USA a = new USA(unsc);
		Iraq b = new Iraq(unsc);
		
		unsc.setUsa(a);
		unsc.setIraq(b);
		
		a.send("不许研制核武器，否则就发动战争！");
		b.send("我们没有核武器，不发动战争！");
		
		
		
	}

}
