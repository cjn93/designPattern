package com.cjn.abstractFactory.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:36:19
*@remark 
*
**/
public class MysqlFactory implements iFactory {

	@Override
	public UserInterface createUser() {
		// TODO Auto-generated method stub
		return new MySqlUser();
	}

}
