package com.cjn.abstractFactory.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:33:06
*@remark 
*
**/
public interface iFactory {
	
	public UserInterface createUser();

}
