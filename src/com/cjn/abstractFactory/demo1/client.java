package com.cjn.abstractFactory.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:37:11
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		
			User user =new User("1002","cjn");
			
			//iFactory factory = new OracleFactory();
			iFactory factory = new MysqlFactory();
			UserInterface iu = factory.createUser();
			
			iu.insertInfo(user);
			iu.getInfo("1002");
			
			
			
			
	}
}
