package com.cjn.abstractFactory.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:14:10
*@remark 
*
**/
public class MySqlUser implements UserInterface {
	
	@Override
	public void insertInfo(User user){
		System.out.println("mysql 向 user 表插入一条记录,info:"+user.toString());
	}
	

	@Override
	public void getInfo(String id) {
		// TODO Auto-generated method stub
		System.out.println("mysql 通过"+id+"，从user表中得到一条记录");
	}
}
