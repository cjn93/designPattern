package com.cjn.abstractFactory.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:30:08
*@remark 
*
**/
public interface UserInterface {
	
	public void insertInfo(User user);
	
	public void getInfo(String id);

}
