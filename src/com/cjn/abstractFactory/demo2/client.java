package com.cjn.abstractFactory.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:37:11
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		
			User user =new User("1002","cjn");
			Department department = new Department("1001", "人事部");
			iFactory factory = new OracleFactory();
			//iFactory factory = new MysqlFactory();
			iUser iu = factory.createUser();
			iDepartment idept = factory.createDepartment();
			
			
			iu.insertInfo(user);
			iu.getInfo("1002");
			
			idept.insertInfo(department);
			idept.getInfo("1001");
			
			
			
			
	}
}
