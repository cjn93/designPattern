package com.cjn.abstractFactory.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:35:28
*@remark 
*
**/
public class OracleFactory implements iFactory{

	@Override
	public iUser createUser() {
		// TODO Auto-generated method stub
		return new OracleUser();
	}

	@Override
	public iDepartment createDepartment() {
		// TODO Auto-generated method stub
		return new OracleDepartment();
	}

}
