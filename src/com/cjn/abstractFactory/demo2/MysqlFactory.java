package com.cjn.abstractFactory.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:36:19
*@remark 
*
**/
public class MysqlFactory implements iFactory {

	@Override
	public iUser createUser() {
		// TODO Auto-generated method stub
		return new MySqlUser();
	}

	@Override
	public iDepartment createDepartment() {
		// TODO Auto-generated method stub
		return new MysqlDepartment();
	}

}
