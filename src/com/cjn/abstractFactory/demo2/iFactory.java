package com.cjn.abstractFactory.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:33:06
*@remark 
*
**/
public interface iFactory {
	
	public iUser createUser();
	
	public iDepartment createDepartment();

}
