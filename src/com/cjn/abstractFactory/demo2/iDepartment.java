package com.cjn.abstractFactory.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:41:59
*@remark 
*
**/
public interface iDepartment {
	
	public void insertInfo(Department department);
	
	public void getInfo(String id);

}
