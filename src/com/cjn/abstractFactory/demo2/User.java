package com.cjn.abstractFactory.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:08:48
*@remark 
*
**/
public class User {
	
	private String id;
	private String name;
	
	public User(String id,String name) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.name = name;
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	//set and get
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + "]";
	}
	
	
}
