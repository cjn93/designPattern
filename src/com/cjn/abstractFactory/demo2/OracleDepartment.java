package com.cjn.abstractFactory.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:43:55
*@remark 
*
**/
public class OracleDepartment implements iDepartment {

	@Override
	public void insertInfo(Department department) {
		// TODO Auto-generated method stub
		System.out.println("oracle 向 department 表插入一条记录,info:"+department.toString());
	}

	@Override
	public void getInfo(String id) {
		// TODO Auto-generated method stub
		System.out.println("oracle 通过"+id+"，从 department 表中得到一条记录");
	}

}
