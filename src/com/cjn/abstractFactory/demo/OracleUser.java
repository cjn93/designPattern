package com.cjn.abstractFactory.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:13:56
*@remark 
*
**/
public class OracleUser {
	
	public void insertInfo(User user){
		System.out.println("oracle 向 user 表插入一条记录,info:"+user.toString());
	}
	
	public void getUserInfo(String id){
		System.out.println("oracle 通过"+id+"，从user表中得到一条记录");
	}
	

}
