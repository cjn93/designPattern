package com.cjn.abstractFactory.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:17:27
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		 User user = new User("1001", "cjn");
		 
		 OracleUser oracle = new OracleUser();
		 
		 oracle.insertInfo(user);
		 
		 oracle.getUserInfo("1001");
		 
	}

}
