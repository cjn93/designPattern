package com.cjn.abstractFactory.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月31日 下午5:14:10
*@remark 
*
**/
public class MySqlUser {
	
	public void insertInfo(User user){
		System.out.println("mysql 向 user 表插入一条记录,info:"+user.toString());
	}
	
	public void getUserInfo(String id){
		System.out.println("mysql 通过"+id+"，从user表中得到一条记录");
	}
}
