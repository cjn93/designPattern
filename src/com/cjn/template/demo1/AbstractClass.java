package com.cjn.template.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年5月29日 下午3:09:49
*@remark 
*
**/
public abstract class AbstractClass {
	
	
	//一些抽象行为，到具体的子类中进行实现
	public abstract void primitiveOperation_1();
	public abstract void primitiveOperation_2();
	
	
	//
	public void templateMethod(){
		primitiveOperation_1();
		primitiveOperation_2();
	}
	

}
