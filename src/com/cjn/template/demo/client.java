package com.cjn.template.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年5月29日 下午2:49:05
*@remark 
*
**/
public class client {
	
	public static void main(String[] args) {
		StudentA a = new StudentA();
		StudentB b = new StudentB();
		
		
		a.testQuestion_1();
		a.testQuestion_2();
		a.testQuestion_3();
		
		b.testQuestion_1();
		b.testQuestion_2();
		b.testQuestion_3();
	
	}

}
