package com.cjn.memento.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午2:34:59
*@remark 
*
**/
public class Originator {
	
	//需要保存的属性，可以有多个
	private String state;
	
	//创建一个备忘录
	public Memento createMemento(){
		return new Memento(this.state);
	}
	//从备忘录中恢复数据
	public void setMementoState(Memento memento){
		this.state = memento.getState();
	}
	
	
	public void showState(){
		System.out.println("当前的state:"+state);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	

}
