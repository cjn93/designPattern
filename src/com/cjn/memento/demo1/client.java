package com.cjn.memento.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午2:55:09
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Originator originator = new Originator();
		System.out.println("设置状态");
		originator.setState("ON");
		originator.showState();
		System.out.println("创建备份");
		CareTaker careTaker = new CareTaker();
		careTaker.setMemento(originator.createMemento());
		System.out.println("设置新状态");
		originator.setState("OFF");
		originator.showState();
		System.out.println("恢复备份状态");
		originator.setMementoState(careTaker.getMemento());
		originator.showState();
		
		
	}

}
