package com.cjn.memento.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午2:43:44
*@remark 
*
**/
public class Memento {
	
	
	private String state;
	
	
	public Memento(String state) {
		// TODO Auto-generated constructor stub
		this.state = state;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}
	
	
	
	

}
