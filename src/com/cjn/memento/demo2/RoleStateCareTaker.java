package com.cjn.memento.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午3:20:33
*@remark 
*
**/
public class RoleStateCareTaker {
	
	private RoleStateMemento memento;

	public RoleStateMemento getMemento() {
		return memento;
	}

	public void setMemento(RoleStateMemento memento) {
		this.memento = memento;
	}

}
