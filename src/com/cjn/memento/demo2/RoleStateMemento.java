package com.cjn.memento.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午3:18:24
*@remark 
*
**/
public class RoleStateMemento {
	
		//生命力
		private int vit;
		//攻击力
		private int ack;
		//防御力
		private int def;
		
		public RoleStateMemento(int vit,int ack,int def) {
			// TODO Auto-generated constructor stub
			this.vit = vit;
			this.ack = ack;
			this.def = def;
		}
		

		//set and get
		public int getVit() {
			return vit;
		}

		public void setVit(int vit) {
			this.vit = vit;
		}

		public int getAck() {
			return ack;
		}

		public void setAck(int ack) {
			this.ack = ack;
		}

		public int getDef() {
			return def;
		}

		public void setDef(int def) {
			this.def = def;
		}
		
		
		

}
