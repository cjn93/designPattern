package com.cjn.memento.demo2;

/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午3:24:25
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GameRole gr = new GameRole();
		
		gr.getInitState();
		
		gr.showState();
		
		System.out.println("存档！！！");
		RoleStateCareTaker taker = new RoleStateCareTaker();
		taker.setMemento(gr.saveState());
		System.out.println("决战BOSS");
		gr.fight();
		gr.showState();
		
		System.out.println("读取存档");
		gr.recoverRoleState(taker.getMemento());
		gr.showState();
		
		
	}

}
