package com.cjn.memento.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午2:17:35
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GameRole gr = new GameRole();
		
		gr.getInitState();
		
		gr.showState();
		//存档，备份
		System.out.println("存档，保存");
		GameRole bak = new GameRole();
		bak.setAck(gr.getAck());
		bak.setDef(gr.getDef());
		bak.setVit(gr.getVit());
		
		System.out.println("与BOSS决斗");
		gr.fight();
		gr.showState();
		
		System.out.println("读取存档，重新来过");
		
		gr.setAck(bak.getAck());
		gr.setDef(bak.getDef());
		gr.setVit(bak.getVit());
		
		gr.showState();
		
		
		
		
	}

}
