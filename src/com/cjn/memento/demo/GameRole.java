package com.cjn.memento.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午2:05:01
*@remark  游戏角色类
*
**/
public class GameRole {
	
	//生命力
	private int vit;
	//攻击力
	private int ack;
	//防御力
	private int def;
	
	//显示当前角色当前状态
	public void showState(){
		System.out.println("角色当前状态：");
		System.out.println("生命力:"+vit);
		System.out.println("攻击力:"+ack);
		System.out.println("防御力:"+def);
	}
	//获得初始状态
	public void getInitState(){
		this.vit = 100;
		this.ack = 100;
		this.def = 100;
	}
	//战斗boss
	public void fight(){
		this.vit = 0;
		this.ack = 0;
		this.def = 0;
	}
	

	//set and get
	public int getVit() {
		return vit;
	}
	public void setVit(int vit) {
		this.vit = vit;
	}
	public int getAck() {
		return ack;
	}
	public void setAck(int ack) {
		this.ack = ack;
	}
	public int getDef() {
		return def;
	}


	public void setDef(int def) {
		this.def = def;
	}
	
	
	
	
	
}
