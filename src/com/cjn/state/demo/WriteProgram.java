package com.cjn.state.demo;

/**
 * @author cjn
 * @version 1.0
 * @date 2018年6月1日 下午3:45:03
 * @remark
 *
 **/
public class WriteProgram {

	static int hour = 0;
	static boolean flag = false;

	public static void writePrograms() {

		if (hour < 12) {
			System.out.println("当前时间："+hour+",上午工作，精神百倍！");
		} else if (hour < 13) {
			System.out.println("当前时间："+hour+",饿了，午饭，午休");
		} else if (hour < 17) {
			System.out.println("当前时间："+hour+",下午状态还不错，继续努力！");
		} else {
			if (flag) {
				System.out.println("当前时间："+hour+",工作完成，下班了");
			} else {
				if (hour < 21) {
					System.out.println("当前时间："+hour+",加班，好累啊！");
				} else {
					System.out.println("当前时间："+hour+",不行了，要睡着了！");
				}

			}
		}

	}
	
	public static void main(String[] args) {
		hour = 9;
		writePrograms();
		hour =12;
		writePrograms();
		hour =13;
		writePrograms();
		hour = 16;
		writePrograms();
		hour =18;
		//flag =true;
		writePrograms();
		hour= 20;
		writePrograms();
		hour =23;
		writePrograms();
	}

}
