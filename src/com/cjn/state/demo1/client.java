package com.cjn.state.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:00:44
*@remark 
*
**/
public class client {
	
	
	public static void main(String[] args) {
		Work work = new Work();
		work.setHour(9);
		work.writePrograms();
		work.setHour(12);
		work.writePrograms();
		work.setHour(13);
		work.writePrograms();
		work.setHour(16);
		work.writePrograms();
		work.setHour(18);
		
		//work.setFlag(true);
		
		work.writePrograms();
		work.setHour(20);
		work.writePrograms();
		work.setHour(23);
		work.writePrograms();
	}

}
