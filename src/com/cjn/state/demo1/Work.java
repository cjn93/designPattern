package com.cjn.state.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午3:58:31
*@remark 
*
**/
public class Work {
	
	private int hour;
	private boolean flag;
	
	
	public void writePrograms() {

		if (hour < 12) {
			System.out.println("当前时间："+hour+",上午工作，精神百倍！");
		} else if (hour < 13) {
			System.out.println("当前时间："+hour+",饿了，午饭，午休");
		} else if (hour < 17) {
			System.out.println("当前时间："+hour+",下午状态还不错，继续努力！");
		} else {
			if (flag) {
				System.out.println("当前时间："+hour+",工作完成，下班了");
			} else {
				if (hour < 21) {
					System.out.println("当前时间："+hour+",加班，好累啊！");
				} else {
					System.out.println("当前时间："+hour+",不行了，要睡着了！");
				}

			}
		}
	}
	
	
	//set and get
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}
