package com.cjn.state.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:10:10
*@remark 
*
**/
public class Context {
	
	private State state;
	
	//对请求做处理，并置换下一个状态
	public void reqest(){
		
		state.handle(this);
	}
	
	public Context(State state) {
		// TODO Auto-generated constructor stub
		//设置初始的状态
		this.state =state;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
		System.out.println("当前状态："+state.getClass().getName());
	}
	
	

}
