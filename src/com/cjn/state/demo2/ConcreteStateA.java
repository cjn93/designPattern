package com.cjn.state.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:14:09
*@remark 
*
**/
public class ConcreteStateA extends State {

	@Override
	public void handle(Context context) {
		// TODO Auto-generated method stub
		//设置下一状态为B
		context.setState(new ConcreteStateB());
	}

}
