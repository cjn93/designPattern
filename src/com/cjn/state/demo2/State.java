package com.cjn.state.demo2;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:09:21
*@remark 
*
**/
public abstract class State {
	
	public abstract void handle(Context context);
	
}
