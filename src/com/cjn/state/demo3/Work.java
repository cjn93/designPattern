package com.cjn.state.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:21:57
*@remark 
*
**/
public class Work {
	
	private int hour;
	private boolean flag;
	private State current;
	
	public Work(State current) {
		// TODO Auto-generated constructor stub
		this.current = current;
	}
	
	public void writeProgram(){
		current.writePrograms(this);
	}
	
	
	
	
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public State getCurrent() {
		return current;
	}
	public void setCurrent(State current) {
		this.current = current;
	}
	
	
	
	
	

}
