package com.cjn.state.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:23:42
*@remark  中午状态类
*
**/
public class Noon extends State {

	@Override
	public void writePrograms(Work work) {
		// TODO Auto-generated method stub
		if (work.getHour() < 13) {
			System.out.println("当前时间："+work.getHour()+",饿了，午饭，午休");
		} else  {
			work.setCurrent(new AfterNoon());
			work.writeProgram();
		}
	}

}
