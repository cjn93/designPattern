package com.cjn.state.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:22:28
*@remark 下午状态类
*
**/
public class AfterNoon extends State {

	@Override
	public void writePrograms(Work work) {
		// TODO Auto-generated method stub
		if (work.getHour() < 17) {
			System.out.println("当前时间："+work.getHour()+",下午状态还不错，继续努力！");
		} else{
			work.setCurrent(new Evening());
			work.writeProgram();
		}
	}

}
