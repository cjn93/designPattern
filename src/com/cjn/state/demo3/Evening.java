package com.cjn.state.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:23:42
*@remark 晚上状态类
*
**/
public class Evening extends State {

	@Override
	public void writePrograms(Work work) {
		// TODO Auto-generated method stub
		if (work.isFlag()) {
			work.setCurrent(new Restset());
			work.writeProgram();
		} else {
			if (work.getHour() < 21) {
				System.out.println("当前时间："+work.getHour()+",加班，好累啊！");
			} else {
				work.setCurrent(new Sleeping());
				work.writeProgram();
			}

		}
	}

}
