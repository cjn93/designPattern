package com.cjn.state.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:00:44
*@remark 
*
**/
public class client {
	
	
	public static void main(String[] args) {
		Work work = new Work(new BeforeNoon());
		work.setHour(9);
		work.writeProgram();
		work.setHour(12);
		work.writeProgram();
		work.setHour(13);
		work.writeProgram();
		work.setHour(16);
		work.writeProgram();
		work.setHour(18);
		
		//work.setFlag(true);
		
		work.writeProgram();
		work.setHour(20);
		work.writeProgram();
		work.setHour(23);
		work.writeProgram();
	}

}
