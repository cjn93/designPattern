package com.cjn.state.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:22:28
*@remark 睡觉状态类
*
**/
public class Sleeping extends State {

	@Override
	public void writePrograms(Work work) {
		// TODO Auto-generated method stub
		System.out.println("当前时间："+work.getHour()+",不行了，要睡着了！");
	}

}
