package com.cjn.state.demo3;
/**
*@author cjn
*@version 1.0
*@date 2018年6月1日 下午4:22:28
*@remark 上午状态类
*
**/
public class BeforeNoon extends State {

	@Override
	public void writePrograms(Work work) {
		// TODO Auto-generated method stub
		if (work.getHour() < 12) {
			System.out.println("当前时间："+work.getHour()+",上午工作，精神百倍！");
		} else {
			work.setCurrent(new Noon());
			work.writeProgram();
		}
	}

}
