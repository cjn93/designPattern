package com.cjn.composite.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午3:52:33
*@remark 
*
**/
public abstract class Company {
	
	protected String name;
	
	public Company(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	public abstract void add(Company c);
	public abstract void remove(Company c);
	public abstract void disPlay(int depth);
	public abstract void lineOfDuty();
	
	

}
