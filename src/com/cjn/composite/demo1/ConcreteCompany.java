package com.cjn.composite.demo1;

import java.util.ArrayList;
import java.util.List;

/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午4:19:24
*@remark 
*
**/
public class ConcreteCompany extends Company {
	
	private List<Company> childList = new ArrayList<>();

	public ConcreteCompany(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void add(Company c) {
		// TODO Auto-generated method stub
		childList.add(c);
	}

	@Override
	public void remove(Company c) {
		// TODO Auto-generated method stub
		childList.remove(c);
	}

	@Override
	public void disPlay(int depth) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			sb.append("-");
		}
		
		System.out.println(sb.toString() + name);
		for (Company component : childList) {
			component.disPlay(depth+2);
		}
	}

	@Override
	public void lineOfDuty() {
		// TODO Auto-generated method stub
		for (Company company : childList) {
			company.lineOfDuty();
		}
	}

}
