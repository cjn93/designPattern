package com.cjn.composite.demo1;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午3:59:45
*@remark 
*
**/
public class HrDepartment extends Company {

	public HrDepartment(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void add(Company c) {
		// TODO Auto-generated method stub
		System.out.println("Cannot add to a leaf");
		
	}

	@Override
	public void remove(Company c) {
		// TODO Auto-generated method stub
		System.out.println("Cannot remove to a leaf");
	}

	@Override
	public void disPlay(int depth) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			sb.append("-");
		}
		
		System.out.println(sb.toString() + name);
	}

	@Override
	public void lineOfDuty() {
		// TODO Auto-generated method stub
		System.out.println(name+",员工招聘培训管理");
	}

}
