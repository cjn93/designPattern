package com.cjn.composite.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午3:52:33
*@remark 
*
**/
public abstract class Component {
	
	protected String name;
	
	public Component(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	public abstract void add(Component c);
	public abstract void remove(Component c);
	public abstract void disPlay(int depth);
	
	

}
