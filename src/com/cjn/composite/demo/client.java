package com.cjn.composite.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午4:32:17
*@remark 
*
**/
public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Composite root = new Composite("root");
		
		root.add(new Leaf("Leaf A"));
		root.add(new Leaf("Leaf B"));
		
		Composite comp = new Composite("Composite X");
		
		comp.add(new Leaf("Leaf XA"));
		comp.add(new Leaf("Leaf XB"));
		
		root.add(comp);
		
		Composite comp2 = new Composite("Composite XX");
		
		comp2.add(new Leaf("Leaf XXA"));
		comp2.add(new Leaf("Leaf XXB"));
		
		comp.add(comp2);
		
		root.add(new Leaf("Leaf C"));
		Leaf ddd = new Leaf("Leaf D");
		root.add(ddd);
		root.disPlay(1);
		
		root.remove(ddd);
		System.out.println("*******删除Leaf D ***************");
		
		root.disPlay(1);
		
	}

}
