package com.cjn.composite.demo;
/**
*@author cjn
*@version 1.0
*@date 2018年6月4日 下午3:59:45
*@remark 
*
**/
public class Leaf extends Component {

	public Leaf(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void add(Component c) {
		// TODO Auto-generated method stub
		System.out.println("Cannot add to a leaf");
		
	}

	@Override
	public void remove(Component c) {
		// TODO Auto-generated method stub
		System.out.println("Cannot remove to a leaf");
	}

	@Override
	public void disPlay(int depth) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			sb.append("-");
		}
		
		System.out.println(sb.toString() + name);
	}

}
